<?php
    require_once '../classes/usuarios.php';
    $u = new Usuario;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">

    <title>Gecor - Basa</title>
</head>
<body class="bg">
    <div class="text-form font-weight-bold m-1">GERÊNCIA DE CONTROLADORIA</div>
    <hr class="bg-warning m-0">
    <div class="d-block d-md-flex">
        <div class="col-4"></div>
        <div class="col-12 col-md-4">
            <form method="POST">
                <div class="text-center text-form mt-1">
                    <h3>Novo Cadastro</h3>
                </div>
                 <!--Php-->
                <?php
                if(isset($_POST['nome']))
                {
                    $nome = addslashes($_POST['nome']);
                    $telefone= addslashes($_POST['telefone']);
                    $gerencia= addslashes($_POST['gerencia']);
                    $email= addslashes($_POST['email']);
                    $senha= addslashes($_POST['senha']);
                    $confirmarsenha= addslashes($_POST['confsenha']);

                    if(!empty($nome) && !empty($telefone) && !empty($email) && !empty($senha) && !empty($confirmarsenha))
                    {
                        $u -> conectar("gecor_login","localhost","root","");
                        if($u->msgErro == "")
                        {
                            if($senha == $confirmarsenha)
                            {
                                if($u -> cadastrar($nome,$telefone,$gerencia,$email,$senha))
                                {
                                    ?>
                                        <div id="msg-sucesso" class="text-center">
                                            Cadastrado com sucesso, <a href="../index.php"><strong>clique para entrar</strong></a>
                                        </div>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <div class="msg-erro" class="text-center">
                                        Email já cadastrado!
                                    </div>
                                    <?php
                                }
                            }
                            else
                            {
                                    ?>
                                        <div class="msg-erro" class="text-center">
                                            Senha e confirmar senha não correspondem.
                                        </div>
                                    <?php
                            }

                        }
                        else
                        {
                            ?>
                                <div class="msg-erro" class="text-center">
                                    <? echo "Erro:".$u->msgErro; ?>
                                </div>
                            <?php
                        }
                    }
                    else
                    {

                        ?>
                            <div class="msg-erro" class="text-center">
                                Preencha todos os campos!
                            </div>
                        <?php
                    }
                }
                ?>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Nome Completo</label>
                    <input type="text" class="form-control" name="nome" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Escreva seu nome completo" required>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Telefone</label>
                    <input type="text" class="form-control" name="telefone" id="exampleInputEmail1" placeholder="Escreva seu telefone" required>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Gerência</label>
                    <select class="form-control" name="gerencia" id="exampleFormControlSelect2">
                        <option class="select">Escolha uma opção</option>
                        <option>GECOR - Gerência</option>
                        <option>GECOR - Coorc</option>
                        <option>GECOR - Codes</option>
                        <option>GECOR - Corec</option>
                        <option>GECOR - Secretaria</option>
                    </select>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail" maxlength="40" required>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputPassword1">Senha</label>
                    <input type="password" class="form-control" name="senha" id="exampleInputPassword1" placeholder="Sua senha" required>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputPassword1">confirmar Senha</label>
                    <input type="password" class="form-control" name="confsenha" id="exampleInputPassword1" placeholder="Confirme sua senha" required>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary col-12 col-md-4">Cadastrar</button>
                </div>
            </form>
            <a href="../index.php" class="text-light">< Voltar</a>
        </div>
        <div class="col-4"></div>
    </div>
    <br/>
    <div class="fixed-bottom bg-rodape text-center text-rodape">
        Desenvolvido por: RENAP - SOLUÇÕES TECNOLÓGICAS
    </div>
</body>
</html>