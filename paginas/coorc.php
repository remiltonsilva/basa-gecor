<!--Desenvolvido por REMILTON MONTEIRO DA SILVA-->
<?php
    session_start();
    if(!isset($_SESSION['id_usuario'])){
        header("location: paginas/coorc.php");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/elitejs.js"></script>

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style-paginas.css">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">

    <title>Gerência de Controladoria </title>
</head>
<body class="bg">
    <div class="text-light">
        <div class="cabecalho fixed-top d-block d-md-flex justify-content-lg-between text-center">
            <div class="img mb-md-0 mt-1 ml-2">
                <img src="../img/logobasapagina.png" alt=""> COORC
            </div>
            <div class="input-group col-md-4 m-2">
                <input type="search" class="form-control" placeholder="Pesquisa..." aria-label="Pesquisa..." aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-info" type="button">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="user3 dropdown pt-2 m-0">
                <div class="nav-link d-flex text-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="">
                        <section>
                               Remilton Monteiro da Silva
                        </section>
                    </div>
                </div>
                <div class="dropdown-menu text-center p-0">
                    <div class="bg-user3 pt-2 text-light">
                        <img src="../img/RemiltonSilva2.png" alt="img-perfil">
                        <h6 class="">Remilton Monteiro da Silva</h6>
                        <div class="text-i">
                            Membro desde Fev. 2000
                        </div>
                        <br>
                    </div>
                    <div class="d-block justify-content-center">
                        <a href="#" class="text-dark">
                            <div class="m-3">Minhas Reservas</div>
                        </a>
                        <a href="#" class="text-dark">
                            <div class="m-3">Clube de Vantagens</div>
                        </a>
                    </div>
                        <div class="dropdown-divider"></div>
                        <div class="d-flex justify-content-between pl-4 pr-4 pb-2">
                            <a href="">
                                <button type="button" class="btn btn-light border"><i class="fa mr-2 fa-cogs" aria-hidden="true"></i>Perfil</button>
                            </a>
                            <a href="../index.php">
                                <button type="button" class="btn btn-light border">Sair<i class="fa ml-2 fa-sign-out" aria-hidden="true"></i></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
        <!--*****************Menus a esquerda*********************-->
        <br/><br/>
        <div class="botoes-comandos d-block d-md-flex mt-3">
            <div class="ativo navbar-toggler d-block d-md-none" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Alterna navegação">
                <i class="fa fa-2x fa-bars ml-3" aria-hidden="true"></i>
            </div>
            <div class="botoes collapse d-lg-block navbar-collapse text-light col-12 col-md-2 m-0 p-0 pr-md-2" id="navbarNavDropdown">
                <a href="../index.php" class="text-light m-0">
                    <div class="active d-flex flex-row text-light m-0 pt-2 pl-3">
                        <i class="fa mt-1 mr-2 fa-home" aria-hidden="true"></i>
                        <p>Início</p>
                    </div>
                </a>
                <div class="b1">
                    <div class="dropdown">
                        <div class="b1-item pt-2 pb-2 pl-3" data-toggle="collapse" data-target="#cadastro">
                            <i class="fa mt-1 mr-3 fa-user-circle-o" aria-hidden="true"></i>
                            Cadastro
                            <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
                        </div>
                        <ul class="collapse text-center mb-0" id="cadastro">
                            <li class="p-1  lista text-center" id="cadastro-cad1"><a class="text-secondary" href="#">Armários</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad2"><a class="text-secondary" href="#">Atividades</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad3"><a class="text-secondary" href="#">Campeonato</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad4"><a class="text-secondary" href="#">Cobradores</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad5"><a class="text-secondary" href="#">Contratos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad6"><a class="text-secondary" href="#">Compras</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad7"><a class="text-secondary" href="#">Diretoria</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad8"><a class="text-secondary" href="#">Entidades</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad9"><a class="text-secondary" href="#">Arquivos Exp./Impot.</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad10"><a class="text-secondary" href="#">Feriados</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad11"><a class="text-secondary" href="#">Financeiro</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad12"><a class="text-secondary" href="#">Materiais</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad13"><a class="text-secondary" href="#">Patrimônio</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad14"><a class="text-secondary" href="#">Pessoas</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad15"><a class="text-secondary" href="#">Reservas</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad16"><a class="text-secondary" href="#">Sedes</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad17"><a class="text-secondary" href="#">Suspensões</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad18"><a class="text-secondary" href="#">Web</a></li>
                        </ul>
                    </div>
                </div>
                <div class="b1">
                    <div class="dropdown">
                        <div class="b1-item pt-2 pb-2 pl-3" data-toggle="collapse" data-target="#movimentacao">
                            <i class="fa mt-1 mr-3 fa-money" aria-hidden="true"></i>
                            Movimentação
                            <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
                        </div>
                        <ul class="collapse text-center mb-0" id="movimentacao">
                            <li class="p-1 pt-2 lista text-center" id="cadastro-cad19"><a class="text-secondary" href="#">Reservas</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad20"><a class="text-secondary" href="#">Financeiro</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad21"><a class="text-secondary" href="#">Compras</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad22"><a class="text-secondary" href="#">Aluguel de Armário</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad23"><a class="text-secondary" href="#">Exames Médicos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad24"><a class="text-secondary" href="#">Empréstimos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad25"><a class="text-secondary" href="#">Clube de Vantagens</a></li>
                        </ul>
                    </div>
                </div>
                <div class="b1 ">
                    <div class="dropdown">
                        <div class="b1-item pt-2 pb-2 pl-3" data-toggle="collapse" data-target="#ferramentas">
                            <i class="fa mt-1 mr-3 fa-wrench" aria-hidden="true"></i>
                            Ferramentas
                            <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
                        </div>
                        <ul class="collapse text-center mb-0" id="ferramentas">
                            <li class="p-1 pt-2 lista text-center" id="cadastro-cad26"><a class="text-secondary" href="#">Mala Direta</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad27"><a class="text-secondary" href="#">Etiqueta</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad28"><a class="text-secondary" href="#">Impressão de Carteirinha</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad29"><a class="text-secondary" href="#">Nova Senha</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad30"><a class="text-secondary" href="#">Direitos de Acesso</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad31"><a class="text-secondary" href="#">Consulta de Log</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad32"><a class="text-secondary" href="#">Grupos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad33"><a class="text-secondary" href="#">Usuários</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad34"><a class="text-secondary" href="#">Importações</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad35"><a class="text-secondary" href="#">Exportações</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad36"><a class="text-secondary" href="#">Contratos Realizados</a></li>
                        </ul>
                    </div>
                </div>
                <div class="b1 ">
                    <div class="dropdown">
                        <div class="b1-item pt-2 pb-2 pl-3" data-toggle="collapse" data-target="#relatorios">
                            <i class="fa mt-1 mr-3 fa-line-chart" aria-hidden="true"></i>
                            Relatórios
                            <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
                        </div>
                        <ul class="collapse text-center mb-0" id="relatorios">
                            <li class="p-1 pt-2 lista text-center" id="cadastro-cad37"><a class="text-secondary" href="#">Associados</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad38"><a class="text-secondary" href="#">Atividades</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad39"><a class="text-secondary" href="#">Reservas</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad40"><a class="text-secondary" href="#">Empréstimos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad41"><a class="text-secondary" href="#">Financeiro</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad42"><a class="text-secondary" href="#">Compras</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad43"><a class="text-secondary" href="#">Armários</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad44"><a class="text-secondary" href="#">Regionais</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad45"><a class="text-secondary" href="#">Patrimônio</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad46"><a class="text-secondary" href="#">Entidades</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad47"><a class="text-secondary" href="#">Exames Médicos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad48"><a class="text-secondary" href="#">Aposentados CEF</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad49"><a class="text-secondary" href="#">Campeonatos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad50"><a class="text-secondary" href="#">Clube de Vantagens</a></li>
                        </ul>
                    </div>
                </div>
                <div class="b1">
                    <div class="dropdown">
                        <div class="b1-item pt-2 pb-2 pl-3" data-toggle="collapse" data-target="#configuracoes">
                            <i class="fa mt-1 mr-3 fa-cogs" aria-hidden="true"></i>
                            Configurações
                            <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
                        </div>
                        <ul class="collapse text-center mb-0" id="configuracoes">
                            <li class="p-1 pt-2 lista text-center" id="cadastro-cad51"><a class="text-secondary" href="#">Sistema</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad52"><a class="text-secondary" href="#">Gatilhos</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad53"><a class="text-secondary" href="#">Configuração Carteirinha</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad54"><a class="text-secondary" href="#">Configuração Etiqueta</a></li>
                        </ul>
                    </div>
                </div>
                <div class="b1 ">
                    <div class="dropdown">
                        <div class="b1-item pt-2 pb-2 pl-3" data-toggle="collapse" data-target="#ajuda">
                            <i class="fa mt-1 mr-3 fa-question-circle" aria-hidden="true"></i>
                            Ajuda
                            <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
                        </div>
                        <ul class="collapse text-center mb-0" id="ajuda">
                            <li class="p-1 pt-2 lista text-center" id="cadastro-cad55"><a class="text-secondary" href="#">Conteúdo</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad56"><a class="text-secondary" href="#">Sobre</a></li>
                            <li class="p-1 lista text-center" id="cadastro-cad57"><a class="text-secondary" href="#">Suporte</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--*****Conteúdo a Direita*****-->
            <div class="comandos col-12 col-lg-10 p-0 border-left text-muted">
                <!--Menus a cima-->
                <div class="comandos_principais">
                    <div class="text-center" id="apresentacao-logo">
                        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                    </div>
                    <div class="comandos_cad collapse" id="cadastro-armarios">
                        <div class="text-center">
                            <h4 class="p-2">CADASTRO DE ARMÁRIOS.</h4>
                        </div>
                        <div class="ativo-2 d-block d-md-none" data-toggle="collapse" data-target="#submenus" aria-controls="submenus" aria-expanded="false">
                            <i class="fa fa-caret-square-o-down float-right m-2" aria-hidden="true"></i>
                            <br><br>
                        </div>
                        <div class="collapse d-md-flex justify-content-center bg-light p-2" id="submenus">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Cadastro de Armários</div> |
                            <div class="cad cad2 ml-3 mr-3" id="cad2">localização</div> |
                            <div class="cad cad3 ml-3 mr-3" id="cad3">Tipos de Armários</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1 collapse" id="form_cad1">
                                <br/>
                                <div class="pesquisa d-block d-md-flex">
                                    <div class="form-group col-12 col-md-3">
                                        <h6>Pesquisar:</h6>
                                        <div class="input-group">
                                            <input type="search" class="form-control" placeholder="Pesquisa..." aria-label="Pesquisa..." aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <h6>Filtar por:</h6>
                                        <select class="form-control" id="FormControlSelectPessoa">
                                            <option>Selecione uma opção</option>
                                            <option>Matrícula/ Nome/ Cognome</option>
                                            <option>Piscina</option>
                                            <option>Churrasqueira</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <h6>Tipo de Contrato:</h6>
                                        <select class="form-control" id="FormControlSelectPessoa">
                                            <option>Selecione uma opção</option>
                                            <option>Todos</option>
                                            <option>Associados</option>
                                            <option>Associados Corportivos</option>
                                            <option>Usuário do Clube</option>
                                            <option>Aluno</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <h6>Situação de contrato:</h6>
                                        <select class="form-control" id="FormControlSelectPessoa">
                                            <option>Selecione uma opção</option>
                                            <option>Todos</option>
                                            <option>Ativo</option>
                                            <option>Ausente</option>
                                            <option>Desligado</option>
                                            <option>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="form-group">
                                    <br class="d-none d-md-block">
                                    <form action="">
                                        <div class="d-block d-md-flex">
                                                <div class="col-12 col-md-4">
                                                    <h6>Código</h6>
                                                    <input type="text" class="form-control" id="formGroupInputCod" placeholder="Código do Armário">
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <h6>Local</h6>
                                                    <select class="form-control" id="FormControlSelectPessoa">
                                                        <option>Selecione uma opção</option>
                                                        <option>Ginásio</option>
                                                        <option>Piscina</option>
                                                        <option>Churrasqueira</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <h6>Tipo</h6>
                                                    <input type="text" class="form-control" id="formGroupInputCod" placeholder="Tipo">
                                                </div>
                                        </div>
                                        <br><br>
                                    </form>
                                    <div class="d-block d-md-flex justify-content-center">
                                            <div class="d-none d-md-flex col-md-3"></div>
                                            <div class="d-block d-md-flex col-12 col-md-6">
                                                <button type="button" class="btn btn-success btn-block d-flex justify-content-center p-2 m-md-2">
                                                    <i class="fa mt-1 mr-2 fa-floppy-o" aria-hidden="true"></i> Salvar
                                                </button>
                                                <button type="button" class="btn btn-danger btn-block d-flex justify-content-center p-2 m-md-2">
                                                    <i class="fa mt-1 mr-2 fa-times" aria-hidden="true"></i> Limpar
                                                </button>
                                                <button type="button" class="btn btn-primary btn-block d-flex justify-content-center p-2 m-md-2">
                                                    <i class="fa mt-1 mr-2 fa-ban" aria-hidden="true"></i> Cancelar
                                                </button>
                                            </div>
                                            <div class="d-none d-md-block col-md-3"></div>
                                    </div>
                                <br>
                                </div>
                            </div>
                            <div class="form-cad2_ collapse" id="form_cad2">
                                <br/>
                                <div class="form-group">
                                    <div class="">
                                        <br>
                                        <form action="">
                                            <div class="d-block d-md-flex">
                                                <div class="col-12 col-md-4">
                                                    <h6>Código</h6>
                                                    <input type="text" class="form-control" id="formGroupInputCod" placeholder="Código do Armário">
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <h6>Descrição</h6>
                                                    <input type="text" class="form-control" id="formGroupInputCod" placeholder="Descrição">
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <h6>Tipo</h6>
                                                    <input type="text" class="form-control" id="formGroupInputCod" placeholder="Tipo">
                                                </div>
                                            </div>
                                            <br><br>
                                        </form>
                                        <div class="d-block d-md-flex justify-content-center">
                                            <div class="d-none d-md-flex col-md-3"></div>
                                            <div class="d-block d-md-flex col-12 col-md-6">
                                            <button type="button" class="btn btn-success btn-block d-flex justify-content-center p-2 m-md-2">
                                                <i class="fa mt-1 mr-2 fa-floppy-o" aria-hidden="true"></i> Salvar
                                            </button>
                                            <button type="button" class="btn btn-info btn-block d-flex justify-content-center p-2 m-md-2">
                                                <i class="fa mt-1 mr-2 fa-times-circle-o" aria-hidden="true"></i> Fechar
                                            </button>
                                            <button type="button" class="btn btn-danger btn-block d-flex justify-content-center p-2 m-md-2">
                                                <i class="fa mt-1 mr-2 fa-times" aria-hidden="true"></i> Limpar
                                            </button>
                                            <button type="button" class="btn btn-primary btn-block d-flex justify-content-center p-2 m-md-2">
                                                <i class="fa mt-1 mr-2 fa-ban" aria-hidden="true"></i> Cancelar
                                            </button>
                                            </div>
                                            <div class="d-none d-md-block col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Atividades-->
                    <div class="comandos_cad collapse" id="cadastro-atividades">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad8 ml-3 mr-3" id="cad8">Cadastro de Atividades</div>
                            <div class="cad cad9 ml-3 mr-3" id="cad2">Dados Corportivos</div>
                            <div class="cad cad10 ml-3 mr-3" id="cad4">End. Comercial</div>
                            <div class="cad cad511ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad12 ml-3 mr-3" id="cad6">Funcionários</div>
                            <div class="cad cad13 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad14_ collapse" id="form_cad8">Teste Dados Cadastrais</div>
                            <div class="form-cad15_ collapse" id="form_cad9">Teste Dados Pessoais</div>
                            <div class="form-cad16_ collapse" id="form_cad10">Teste End. Residencial</div>
                            <div class="form-cad17_ collapse" id="form_cad11">Teste End. Comercial</div>
                            <div class="form-cad18_ collapse" id="form_cad12">Teste Cobrança</div>
                            <div class="form-cad19_ collapse" id="form_cad13">Teste Aposentados</div>
                            <div class="form-cad20_ collapse" id="form_cad14">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Campeonato-->
                    <div class="comandos_cad collapse" id="cadastro-campeonato">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad21 ml-3 mr-3" id="cad18">Campeonatos</div>
                            <div class="cad cad22 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad23 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad24 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad25 ml-3 mr-3" id="cad6">Responsável</div>
                            <div class="cad cad27 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad28_ collapse" id="form_cad15">Teste Dados Cadastrais</div>
                            <div class="form-cad29_ collapse" id="form_cad16">Teste Dados Pessoais</div>
                            <div class="form-cad30_ collapse" id="form_cad17">Teste End. Residencial</div>
                            <div class="form-cad31_ collapse" id="form_cad18">Teste End. Comercial</div>
                            <div class="form-cad32_ collapse" id="form_cad19">Teste Cobrança</div>
                            <div class="form-cad33_ collapse" id="form_cad20">Teste Aposentados</div>
                            <div class="form-cad34_ collapse" id="form_cad21">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Cobradores-->
                    <div class="comandos_cad collapse" id="cadastro-cobradores">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Cadastro de Cobradores</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad22">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad23">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad24">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad25">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad26">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad27">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad28">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Contratos-->
                    <div class="comandos_cad collapse" id="cadastro-contratos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Cadastro de Contrato</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad29">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad30">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad31">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad32">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad33">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad34">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad35">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Compras-->
                    <div class="comandos_cad collapse" id="cadastro-compras">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Cadastro de Compras</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad36">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad37">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad38">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad39">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad40">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad41">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad42">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Diretoria-->
                    <div class="comandos_cad collapse" id="cadastro-diretoria">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Cadastrais de Diretoria</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad43">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad44">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad45">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad46">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad47">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad48">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad49">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Entidades-->
                    <div class="comandos_cad collapse" id="cadastro-entidades">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Cadastrais de Entidades</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad50">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad51">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad52">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad53">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad54">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad55">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad56">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Arquivos de Exportação e Importação-->
                    <div class="comandos_cad collapse" id="cadastro-arquivoExpImport">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Exportação e Importação</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad57">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad58">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad59">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad60">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad61">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Feriados-->
                    <div class="comandos_cad collapse" id="cadastro-feriados">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Feriados</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Financeiro-->
                    <div class="comandos_cad collapse" id="cadastro-financeiro">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Financeiro</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Materiais-->
                    <div class="comandos_cad collapse" id="cadastro-materiais">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Materiais</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Patrimônio-->
                    <div class="comandos_cad collapse" id="cadastro-patrimonio">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Patrimônio</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Pessoas-->
                    <div class="comandos_cad collapse" id="cadastro-pessoas">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Pessoas</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Reservas-->
                    <div class="comandos_cad collapse" id="cadastro-reservas">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Reservas</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Sedes-->
                    <div class="comandos_cad collapse" id="cadastro-sedes">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Sedes</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Suspensões-->
                    <div class="comandos_cad collapse" id="cadastro-suspensoes">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Suspensões</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Web-->
                    <div class="comandos_cad collapse" id="cadastro-web">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CADASTRO DE PESSOAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">web</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">End. Residencial</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Visitante</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Movimantação-->
                    <!--Menu Reservas-->
                    <div class="comandos_cad collapse" id="movimentacao-reservas">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Dados da Reserva</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Movimantação</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Financeiro-->
                    <div class="comandos_cad collapse" id="movimentacao-financeiro">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Dados da Financeiros</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Compras-->
                    <div class="comandos_cad collapse" id="movimentacao-compras">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Compras</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Aluguel de Armário-->
                    <div class="comandos_cad collapse" id="movimentacao-aluguel-armario">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Aluguéis de Armários</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Exames Médicos-->
                    <div class="comandos_cad collapse" id="movimentacao-exames-medicos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Exames Médicos</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Empréstimos-->
                    <div class="comandos_cad collapse" id="movimentacao-emprestimos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Empréstimos</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Clube de Vantagens-->
                    <div class="comandos_cad collapse" id="movimentacao-clubevantagens">
                        <div class="col-12 text-center">
                            <h4 class="p-2">MOVIMENTAÇÃO.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Clube de Vantagens</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Ferramentas-->
                    <!--Menu Mala Direta-->
                    <div class="comandos_cad collapse" id="ferramentas-mala-direta">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Mala Direta</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Etiqueta-->
                    <div class="comandos_cad collapse" id="ferramentas-etiqueta">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Etiqueta</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Carteirinha-->
                    <div class="comandos_cad collapse" id="ferramentas-carteirinha">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Carteirinha</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Nova Senha-->
                    <div class="comandos_cad collapse" id="ferramentas-nova-senha">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Nova Senha</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Acesso-->
                    <div class="comandos_cad collapse" id="ferramentas-acesso">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Direitos de Acesso</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Consulta de log-->
                    <div class="comandos_cad collapse" id="ferramentas-consulta-log">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Consulta de Log</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Grupos-->
                    <div class="comandos_cad collapse" id="ferramentas-grupos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Grupos</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Usuários-->
                    <div class="comandos_cad collapse" id="ferramentas-usuarios">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Usuários</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Importações-->
                    <div class="comandos_cad collapse" id="ferramentas-importacoes">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Importações</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Exportações-->
                    <div class="comandos_cad collapse" id="ferramentas-exportacoes">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Exportações</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Contratos Realizados-->
                    <div class="comandos_cad collapse" id="ferramentas-contratos-realizados">
                        <div class="col-12 text-center">
                            <h4 class="p-2">FERRAMENTAS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Contratos Realizados</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Relatórios-->
                    <!--Menu Associados-->
                    <div class="comandos_cad collapse" id="relatorios-associados">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Relatórios de Associados</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Atividades-->
                    <div class="comandos_cad collapse" id="relatorios-atividades">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Atividades</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Reservas-->
                    <div class="comandos_cad collapse" id="relatorios-reservas">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Reservas</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Empréstimo-->
                    <div class="comandos_cad collapse" id="relatorios-emprestimo">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Empréstimo</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Financeiro-->
                    <div class="comandos_cad collapse" id="relatorios-financeiro">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Financeiro</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Compras-->
                    <div class="comandos_cad collapse" id="relatorios-compras">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Compras</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Armários-->
                    <div class="comandos_cad collapse" id="relatorios-armarios">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Armários</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Regionais-->
                    <div class="comandos_cad collapse" id="relatorios-regionais">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Regionais</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Patrimônio-->
                    <div class="comandos_cad collapse" id="relatorios-patrimonio">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Patrimônio</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Entidades-->
                    <div class="comandos_cad collapse" id="relatorios-entidades">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Entidades</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Exames Médicos-->
                    <div class="comandos_cad collapse" id="relatorios-exames-medicos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Exames Médicos</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Aposentados CEF-->
                    <div class="comandos_cad collapse" id="relatorios-aposentaos-cef">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Aposentados CEF</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Campeonatos-->
                    <div class="comandos_cad collapse" id="relatorios-campeonatos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Campeonatos</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Menu Clube de Vantagens-->
                    <div class="comandos_cad collapse" id="relatorios-clubedevantagens">
                        <div class="col-12 text-center">
                            <h4 class="p-2">RELATÓRIOS.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Clube de Vantagens</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Teste Dados Cadastrais</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Configurações-->
                    <!--Sistema-->
                    <div class="comandos_cad collapse" id="configuracoes-sistema">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Configuração do Sistema</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Gatilhos-->
                    <div class="comandos_cad collapse" id="configuracoes-gatilhos">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Configuração de Gatilho</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Carteirinha-->
                    <div class="comandos_cad collapse" id="configuracoes-carteirinha">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Configuraçãode Carteirinha</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Etiqueta-->
                    <div class="comandos_cad collapse" id="configuracoes-etiqueta">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Configuração de Etiqueta</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Ajuda-->
                    <!--Conteúdo-->
                    <div class="comandos_cad collapse" id="ajuda-conteudo">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Ajuda Conteúdo</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Sobre-->
                    <div class="comandos_cad collapse" id="ajuda-sobre">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Ajuda Sobre</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                    <!--Suporte-->
                    <div class="comandos_cad collapse" id="ajuda-suporte">
                        <div class="col-12 text-center">
                            <h4 class="p-2">CONFIGURAÇÕES.</h4>
                        </div>
                        <div class="d-flex justify-content-center bg-light p-2">
                            <div class="cad cad1 ml-3 mr-3" id="cad1">Ajuda Suporte</div>
                            <div class="cad cad2 ml-3 mr-3" id="cad2">Dados Pessoais</div>
                            <div class="cad cad4 ml-3 mr-3" id="cad4">Tipo de Reserva</div>
                            <div class="cad cad5 ml-3 mr-3" id="cad5">Cobrança</div>
                            <div class="cad cad7 ml-3 mr-3" id="cad7">Outros</div>
                        </div>
                        <div class="form-cad">
                            <div class="form-cad1_ collapse" id="form_cad1">Configuração de Sistema</div>
                            <div class="form-cad2_ collapse" id="form_cad2">Teste Dados Pessoais</div>
                            <div class="form-cad3_ collapse" id="form_cad3">Teste End. Residencial</div>
                            <div class="form-cad4_ collapse" id="form_cad4">Teste End. Comercial</div>
                            <div class="form-cad5_ collapse" id="form_cad5">Teste Cobrança</div>
                            <div class="form-cad6_ collapse" id="form_cad6">Teste Aposentados</div>
                            <div class="form-cad7_ collapse" id="form_cad7">Teste Outros</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fixed-bottom">
            <div class="col-12 d-block text-center d-md-flex justify-content-between">
                <div class="copyright d-flex">
                    <div>
                        <p class="p-1 text-light">Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2019</p>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="text-light mr-2">
                        <?php
                            date_default_timezone_set('America/Sao_Paulo');
                            $date = date('d-m-Y');
                            echo $date;
                        ?>
                    </div>
                    <div class="text-light ml-2">    
                        <?php
                            date_default_timezone_set('America/Sao_Paulo');
                            $date = date('H:i');
                            echo $date;
                        ?>
                    </div>
                </div>
                <div class="redessociais">
                    <a class="text-light" href="#">
                        <p>Gecor - Gerência de Controladoria</p>
                    </a>
                </div>
            </div>
        </div>
    </div>    
</body>
</html>