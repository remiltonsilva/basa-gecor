<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">

    <title>Gecor - Basa</title>
</head>
<body class="bg">
    <div class="text-form font-weight-bold mt-3">GERÊNCIA DE CONTROLADORIA</div>
    <hr class="bg-warning">
    <div class="d-block d-md-flex">
        <div class="col-4"></div>
        <div class="col-12 col-md-4">
            <form method="POST" class="mt-md-1">
                <div class="text-center text-form">
                    <h3>Novo Cadastro</h3>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Nome Completo</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Escreva seu nome completo">
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Telefone</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Escreva seu telefone">
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Gerência</label>
                    <select class="form-control" id="exampleFormControlSelect2">
                        <option class="select">Escolha uma opção</option>
                        <option>GECOR</option>
                        <option>GECIN</option>
                        <option>SECRE</option>
                        <option>DIREC</option>
                        <option>GECON</option>
                    </select>
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Escreva email">
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputPassword1">Senha</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Sua senha">
                </div>
                <div class="form-group text-form">
                    <label for="exampleInputPassword1">confirmar Senha</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirme sua senha">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary col-12 col-md-4">Cadastrar</button>
                </div>
            </form>
            <a href="../index.php" class="text-light">< Voltar</a>
        </div>
        <div class="col-4"></div>
    </div>
    <div class="fixed-bottom bg-rodape text-center text-rodape">
        Desenvolvido por: RENAP - SOLUÇÕES TECNOLÓGICAS
    </div>
</body>
</html>