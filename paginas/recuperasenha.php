<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">

    <title>Gecor - Basa</title>
</head>
<body class="bg">
    <p class="m-2 text-light">GECOR - GERÊNCIA DE CONTROLADORIA</p>
    <hr class="bg-warning m-0">
    <div class="d-block d-md-flex">
        <div class="col-md-4"></div>
        <div class="col-12 col-md-4">
            <div><br><br><br><br></div>
            <form method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email Cadastrado</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Senha Antiga</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Senha Antiga">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Senha Nova</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nova Senha">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Repita a Senha Nova</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Repita a Senha">
                </div>
                <button type="submit" class="btn btn-primary">Recuperar</button>
            </form>
            <a href="../index.php">< Voltar</a>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="fixed-bottom bg-rodape text-center text-rodape">
        Desenvolvido por: RENAP - SOLUÇÕES TECNOLÓGICAS
    </div>
</body>
</html>