<?php
    class Usuario{
        private $pdo;
        public $msgErro = "";
        public function conectar($nome,$host,$usuario,$senha)
        {
            global $pdo;
            global $msgErro;
            try
            {
                $pdo = new PDO("mysql:dbname=" .$nome. ";host=" .$host, $usuario,$senha);
            }catch (PDOException $e){
              $msgErro = $e->getMessage();
            }
        }
        public function cadastrar($nome,$telefone,$gerencia,$email,$senha)
        {
            global $pdo;
            $sql = $pdo -> prepare ("SELECT id_usuario FROM usuarios WHERE email = :e");
            $sql->bindValue(":e",$email);
            $sql->execute();
            if($sql->rowCount() > 0)
            {
                return false;
            }
            else{
                $sql = $pdo->prepare("INSERT INTO usuarios (nome,telefone,gerencia,email,senha) VALUES (:n, :t, :g, :e, :s)");
                $sql -> bindValue(":n",$nome);
                $sql -> bindValue(":t",$telefone);
                $sql -> bindValue(":g",$gerencia);
                $sql -> bindValue(":e",$email);
                $sql -> bindValue(":s",md5($senha));
                $sql -> execute();
                return true;
            }
        }
        public function logar($email,$senha)
        {
            global $pdo;
            $sql = $pdo->prepare("SELECT id_usuario FROM usuarios WHERE email = :e AND senha = :s");
            $sql->bindValue(":e",$email);
            $sql->bindValue(":s",md5($senha));
            $sql->execute();
            if($sql->rowCount() > 0)
            {
                $dado = $sql->fetch();
                session_start();
                $_SESSION['id_usuario'] = $dado['id_usuario'];
                return true;
            }
            else
            {
                return false;
            }
        }
        public function logingerencia ($gerencia)
        {
            $sql = $pdo->prepare("SELECT gerencia FROM usuarios WHERE email = :e AND senha = :s");
            $sql -> bindValue(":g",$gerencia);
            $sql -> bindValue(":e",$email);
            $sql -> bindValue(":s",md5($senha));
            $sql -> execute();   
        }
        /*public function cadastrardespesas($parecer,$datap,$mandante,$gestor,$objeto,$obs,$contmensal,$periodo,$qntmeses,$valorcomp)
        {
            global $pdo;
            $sql = $pdo->prepare("SELECT id_despesas FROM despesas2 WHERE parecer = :p");
            $sql->bindValue(":p",$parecer);
            $sql->execute();
            if($sql->rowCount() > 0)
            {
                return false;
            }
            else{
                $sql = $pdo->prepare("INSERT INTO despesas2 (parecer,datap,mandante,gestor,objeto,obs,contmensal,periodo,qntmeses,valorcomp) VALUES (:p, :d, :m, :g, :o, :ob, :c, :pr, :qt, :v)");
                $sql -> bindValue(":p",$parecer);
                $sql -> bindValue(":d",$datap);
                $sql -> bindValue(":m",$mandante);
                $sql -> bindValue(":g",$gestor);
                $sql -> BindValue(":o",$objeto);
                $sql -> BindValue(":ob",$obs);
                $sql -> bindValue(":c",$contmensal);
                $sql -> bindValue(":pr",$periodo);
                $sql -> bindValue(":qt",$qntmeses);
                $sql -> BindValue(":v",$valorcomp);
                $sql -> execute();
                return true;
            }
        }*/
    }
?>
