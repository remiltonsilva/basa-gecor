<?php
    require_once 'classes/usuarios.php';
    $u = new Usuario;
?>
 <?php
            if(isset($_POST['email']))
            {
                $coorc = "GECOR - Coorc";
                $codes = "GECOR - Codes";
                $corec = "GECOR - Corec";
                $secretaria = "GECOR - Secretaria";
                $email= addslashes($_POST['email']);
                $gerencia= addslashes($_POST['gerencia']);
                $senha= addslashes($_POST['senha']);

                if(!empty($email) && !empty($senha))
                {
                    $u->conectar("gecor_login","localhost","root","");
                    if($u->msgErro == "")
                    {
                        if($u->logar($email,$senha))
                        {
                            if($coorc == $gerencia){
                                header("location: paginas/coorc.php");
                            }
                            if($codes == $gerencia){
                                header("location: paginas/codes.php");
                            }
                            if($corec == $gerencia){
                                header("location: paginas/corec.php");
                            }
                            else{
                                echo "Gerência incorreta";
                            }
                        }
                        else
                        {     ?>
                                <div class="msg-erro">
                                    Email e/ou senha estão incorretos!
                                </div>
                            <?php
                        }
                    }
                    else
                    {       ?>
                                <div class="msg-erro">
                                    <?php echo "Erro:" .$u->msgErro; ?>
                                </div>
                            <?php
                    }

                }
                else
                {
                    ?>
                        <div class="msg-erro">
                            Preencha todos os campos!
                        </div>
                    <?php
                }
            }
            ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GECOR - BANCO DA AMAZÔNIA</title>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body class="bg">

    <div class="text-right m-3">
        <a class="p-2 bg-light rounded link-a" data-toggle="modal" data-target="#exampleModal">
        Gerência Gecor
        </a>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Login Gerência</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="email" class="form-control" id="email" required placeholder="Digite seu E-mail">
                        <br/>
                        <input type="password" class="form-control" id="password" placeholder="Digite sua Senha">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-block btn-primary">Entrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container text-light">
        <div class="login d-block d-md-flex text-center">
            <div class="col-md-3"></div>
            <div class="login-elite-1 col-12 col-md-2 d-block">
                <div class="d-block d-md-flex justify-content-center">
                    <img src="img/logobasa.png" alt="">
                    <div>
                        <p class="p2">GECOR</p>
                        <p class="p3">Gerência de Controlodaria </p>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
            <div class="login-elite-2 col-12 col-md-4">
                <form method="POST" class="form-group ml-md-5">
                    <input type="email" class="form-control" name="email" placeholder="E-mail">
                    <br>
                    <select class="form-control" name="gerencia" id="exampleFormControlSelect2">
                        <option class="select">Escolha uma opção</option>
                        <option>GECOR - Gerência</option>
                        <option>GECOR - Coorc</option>
                        <option>GECOR - Codes</option>
                        <option>GECOR - Corec</option>
                        <option>GECOR - Secretaria</option>
                    </select>
                    <br>
                    <input type="password" class="form-control" name="senha" placeholder="Senha">
                    <br>
                    <button type="submit" class="btn btn-info btn-block mb-4" id="entar">Entrar</button>
                    <a class="text-light" href="paginas/recuperasenha.php">Para recuperar sua senha, clique aqui.</a> 
                </form>
            </div>
            <div class="col-md-3"></div>
                </div>
            </div>
            <div class="fixed-bottom bg-rodape text-center text-rodape">
                Desenvolvido por: RENAP - SOLUÇÕES TECNOLÓGICAS
            </div>
</body>
</html>